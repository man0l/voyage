<?php

namespace Response\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Doctrine\ORM\Query\Expr\Join;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Offer");
    	$offers = $rep->findAll();
    	
        return array('offers' => $offers,'destination' => false);
    }
    
    /**
     * @Route("/offers/{destinationId}/{type}", name="offer_destination", defaults={"type"="all"})
     * @Template("ResponseFrontBundle:Default:index.html.twig") 
     */
    public function listOffersAction($destinationId, $type)
    {

    	  
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Offer");
    	
    	if($type == "all")
    	{	    	
	    	$offers = $rep->findBy(array('destinationId' => $destinationId));	    	
    	} else
    	{
    		$qb = $rep->createQueryBuilder("o");
    		$qb
    			->select("o")    			
    			//->where($qb->expr()->like('о.name', "%автобус%"))
    			->where("o.name LIKE :name")    	
    			->andWhere($qb->expr()->eq('o.destination', $destinationId))
    			->setParameter("name", "%$type%")		
    		;
    		
    		$offers = $qb->getQuery()->getResult();
    	} 
    		
    	
    		
    	
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Destination");
    	$destination = $rep->find($destinationId);
    	
    	return array('offers' => $offers, 'destination' => $destination);
    }
    
    
    /**
     * @Route("/offer/details/{id}", name="offer_details")
     * @Template()
     */
    public function detailsAction($id) 
    {
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Offer");
    	$offer = $rep->find($id);
    	
    	if(!$offer)
    		throw new NotFoundHttpException("The offer is not found");
    	
    	return array('offer' => $offer);
    }
    
    
    
    /**
     * @Route("/menu_destinations")
     * @Template()
     * @return multitype:multitype:
     */
    public function destinationsAction() 
    {
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Destination");
    	$destinations = $rep->findAll();
    	
    	return array('destinations' => $destinations);
    }
    
    /**
     * @Route("/hotel/{id}", name="hotel_details")
     * @Template()
     */
    public function hotelDetailsAction($id)
    {
    	$rep = $this->getDoctrine()->getRepository("ResponseParserBundle:Hotel");
    	$repPrices = $this->getDoctrine()->getRepository('ResponseParserBundle:Prices');
    	$em = $this->getDoctrine()->getManager();
    	
    	$hotel = $rep->find($id);
    	
    	if(!$hotel)
    		throw new NotFoundHttpException("The offer is not found");
    	
    	/*
    	$query = $em->createQuery("SELECT DISTINCT p.roomType FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1");
    	$query->setParameter(1, $id);
    	$stmt = $query->execute();
    	$roomTypes = $query->getResult();
    	//print_r($roomTypes);
    	
    	foreach($roomTypes as $rType) {
    		$prices[] = $repPrices->findBy(array('hotelId' => $hotel->getId(), 'roomType' => $rType ));

    		$query = $em->createQuery("SELECT DISTINCT p.title FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1 AND p.roomType = ?2");
    		$query->setParameter(1, $id);
    		$query->setParameter(2, $rType);
    		$stmt = $query->execute();
    		$titles[]  = $query->getResult();
    		
    		$query = $em->createQuery("SELECT DISTINCT p.dateAt FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1 AND p.roomType = ?2");
    		$query->setParameter(1, $id);
    		$query->setParameter(2, $rType);
    		$stmt = $query->execute();
    		$allDates = $query->getResult();
    		
    		 
    	 	$stmt = $em->getConnection()->prepare("SELECT DISTINCT date_at 
    	 										 FROM Prices WHERE hotel_id = '$id'
    	 										 AND room_type = '$rType[roomType]'");
    		$stmt->execute();
    		$allDates = $stmt->fetchAll();
    		
    		$prices = array();
    		
    		foreach($allDates as $date)
    		{	
    			//print_r($date['date']->format("Y-m-d"));
    			 $dateString = $date['date_at'];
    			 
    			  $query = $em->createQuery("SELECT p.link, p.price, p.priceIndex FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1 AND p.roomType = ?2 AND p.dateAt = ?3");
    			 $query->setParameter(1, $hotel->getId());
    			 $query->setParameter(2, $rType['roomType']);
    			 $query->setParameter(3, $date['date_at']);
    			 
    			 $prices[] = $query->getResult(); 
    			 
    			 
    			 
    			 
    			$allPrices[] = $prices;
    		} 
    		 
    		
    		 
    		
    		$dates[] = $allDates;
    		
    	}
    	
    	*/
    	/* SELECT COUNT( DISTINCT room_index )
    	FROM  `prices` */

    	$repPrices = $em->getRepository("ResponseParserBundle:Prices");
    	
    	$query = $em->createQuery("SELECT COUNT(DISTINCT p.roomIndex) FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1");
    	
    	
    	$query->setParameter(1, $id);    	
    	$stmt = $query->execute();
    	$roomIndexes = $query->getScalarResult();
    	
    	if($roomIndexes) {
    		$roomIndexCount = $roomIndexes[0][1];
    	 	
    		for($i = 0; $i < $roomIndexCount; $i++)
    		{
    			// inner cycle with dates
    			
    			$query = $em->createQuery("SELECT p.dateAt FROM ResponseParserBundle:Prices p WHERE p.roomIndex = ?1 AND p.hotelId = ?2 GROUP BY p.dateAt");
    			$query->setParameter(1, $i);
    			$query->setParameter(2, $id);
    			
    			$dates = $query->getResult();
    			
    			$j = 0;
    			foreach($dates as $date) {
    			
	    			$query = $em->createQuery("SELECT p.roomIndex, p.link, p.price, p.priceIndex, p.title, p.roomType, p.dateAt FROM ResponseParserBundle:Prices p WHERE p.hotelId = ?1 AND p.roomIndex = ?2 AND p.dateAt = ?3");
	    			$query->setParameter(1, $hotel->getId());
	    			$query->setParameter(2, $i);
	    			$query->setParameter(3, $date['dateAt']);
	    			
	    			$prices[$i][$j] = $repPrices->findBy(array(
	    				'hotelId' => $id,
	    				'roomIndex' => $i,
	    				'dateAt' => $date['dateAt']	    					
	    			));
	    			
	    			//$prices[$i][$j] = $query->getResult();
	    			
	    			/* foreach($prices[$i][$j] as $priceLink) {
		    			$link = unserialize($prices[$i][$j]['link']);
		    			$link = "http://pochivki.xn----ctbrbbjzhptl1be6j.com/reservation_poc.php?";
		    			$link =  	implode("&",$link);
		    			$prices[$i][$j]['link'] = $link;
	    			} */
	    			$j++;
    			}
    		}
    		
    		 
    	}
    	   
    	
    	
    	return array('hotel' => $hotel, 'prices' => isset($prices) ? $prices : null);
    }
    
    
    /**
     * @Route("/main-menu")
     * @Template()
     */
    function menuAction() 
    {
    	// get offers by different types - bus, own transport, flight from sofia, flight from varna etc.
    	
    	$em = $this->getDoctrine()->getManager();
    	$rep = $em->getRepository("ResponseParserBundle:Offer");
    	
    	$qb = $rep->createQueryBuilder("p");    	
    	$query = $qb->where("p.name LIKE :name")
    		->setParameter("name", '%автобус%')
    		->groupBy("p.destination")
    		->getQuery();    	
    	
    	$bus = $query->getResult();
    	
    	$qb = $rep->createQueryBuilder("p");    	 
    	$query = $qb->where("p.name LIKE :name")
    	->setParameter("name", '%самолет от варна%')
    	->groupBy("p.destination")
    	->getQuery();
    	    	 
    	$flightVarna = $query->getResult();
    	
    	$qb = $rep->createQueryBuilder("p");    	
    	$query = $qb->where("p.name LIKE :name")
    	->setParameter("name", '%самолет от софия%')
    	->groupBy("p.destination")
    	->getQuery();
    	    	
    	$flightSofia = $query->getResult();
    	 
    	$qb = $rep->createQueryBuilder("p");
    	$query = $qb->where("p.name LIKE :name")
    	->setParameter("name", '%собствен%')
    	->groupBy("p.destination")
    	->getQuery();
    	
    	$ownTransport = $query->getResult();  	 
    	
    	
    	return array('bus' => $bus, 'flightVarna' => $flightVarna,  'flightSofia'=> $flightSofia, 'ownTransport' => $ownTransport);
    }
    
}
