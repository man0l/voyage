<?php

namespace Response\ParserBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Response\ParserBundle\Entity\WebDocument;

/**
 * WebDocument controller.
 *
 * @Route("/webdocument")
 */
class WebDocumentController extends Controller
{

    /**
     * Lists all WebDocument entities.
     *
     * @Route("/", name="webdocument")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ResponseParserBundle:WebDocument')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a WebDocument entity.
     *
     * @Route("/{id}", name="webdocument_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ResponseParserBundle:WebDocument')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WebDocument entity.');
        }

        return array(
            'entity'      => $entity,
        );
    }
}
