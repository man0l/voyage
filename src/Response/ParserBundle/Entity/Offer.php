<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Offer
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Response\ParserBundle\Entity\OfferRepository")
 */
class Offer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="destination_id", type="integer", nullable=true)
     */
    private $destinationId;
    
    /**
     * @ORM\ManyToOne(targetEntity="Destination", inversedBy="offers")
     * @var unknown
     */
    private $destination;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

 
    /**
     * @ORM\Column(name="textOffer", type="text", nullable=true)
     * @var unknown
     */
    public $textOffer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_at", type="datetime", nullable=true)
     */
    private $dateAt;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255)
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;
    
    /**
     * @ORM\Column(name="remote_id", type="integer", nullable=true)
     * @var unknown
     */
    private $remoteId; 
    
    /**
     * @ORM\Column(name="days", type="string", length=255, nullable=true)
     * @var unknown
     */
    private $days;
    
    /**
     * @ORM\Column(name="offer_more_text", type="text", nullable=true)
     * @var unknown
     */
    private $offerMoreText;
    
    /**
     * @ORM\OneToMany(targetEntity="OfferImages", mappedBy="offer")
     * @var unknown
     */
    private $images;
    
    /**
     * @ORM\OneToMany(targetEntity="Hotel", mappedBy="offer")
     * @var unknown
     */
    private $hotels;
    
    /**
     * @ORM\Column(name="url", type="text", nullable=true)
     * @var unknown
     */
    private $url;
    
    
    /**
     * @ORM\Column(name="is_parsed", type="boolean", nullable=true)
     * @var unknown
     */
    private $isParsed;

   /**
    *
    * @ORM\Column(name="offer_text", type="text", nullable=true)
    *
    */
    private $offerText;
   

    public function __construct()
    {
    	$this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set destinationId
     *
     * @param integer $destinationId
     * @return Offer
     */
    public function setDestinationId($destinationId)
    {
        $this->destinationId = $destinationId;

        return $this;
    }

    /**
     * Get destinationId
     *
     * @return integer 
     */
    public function getDestinationId()
    {
        return $this->destinationId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Offer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

     
    /**
     * Set dateAt
     *
     * @param \DateTime $dateAt
     * @return Offer
     */
    public function setDateAt($dateAt)
    {
        $this->dateAt = $dateAt;

        return $this;
    }

    /**
     * Get dateAt
     *
     * @return \DateTime 
     */
    public function getDateAt()
    {
        return $this->dateAt;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Offer
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Offer
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set destination
     *
     * @param \Response\ParserBundle\Entity\Destination $destination
     * @return Offer
     */
    public function setDestination(\Response\ParserBundle\Entity\Destination $destination = null)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return \Response\ParserBundle\Entity\Destination 
     */
    public function getDestination()
    {
        return $this->destination;
    }


    /**
     * Set textOffer
     *
     * @param string $textOffer
     * @return Offer
     */
    public function setTextOffer($textOffer)
    {
        $this->textOffer = $textOffer;

        return $this;
    }

    /**
     * Get textOffer
     *
     * @return string 
     */
    public function getTextOffer()
    {
        return $this->textOffer;
    }

    /**
     * Set remoteId
     *
     * @param integer $remoteId
     * @return Offer
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return integer 
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }

    /**
     * Set days
     *
     * @param string $days
     * @return Offer
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * Get days
     *
     * @return string 
     */
    public function getDays()
    {
        return $this->days;
    }

    /**
     * Set offerMoreText
     *
     * @param string $offerMoreText
     * @return Offer
     */
    public function setOfferMoreText($offerMoreText)
    {
        $this->offerMoreText = $offerMoreText;

        return $this;
    }

    /**
     * Get offerMoreText
     *
     * @return string 
     */
    public function getOfferMoreText()
    {
        return $this->offerMoreText;
    }

    /**
     * Add images
     *
     * @param \Response\ParserBundle\Entity\OfferImages $images
     * @return Offer
     */
    public function addImage(\Response\ParserBundle\Entity\OfferImages $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Response\ParserBundle\Entity\OfferImages $images
     */
    public function removeImage(\Response\ParserBundle\Entity\OfferImages $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add hotels
     *
     * @param \Response\ParserBundle\Entity\Hotel $hotels
     * @return Offer
     */
    public function addHotel(\Response\ParserBundle\Entity\Hotel $hotels)
    {
        $this->hotels[] = $hotels;

        return $this;
    }

    /**
     * Remove hotels
     *
     * @param \Response\ParserBundle\Entity\Hotel $hotels
     */
    public function removeHotel(\Response\ParserBundle\Entity\Hotel $hotels)
    {
        $this->hotels->removeElement($hotels);
    }

    /**
     * Get hotels
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHotels()
    {
        return $this->hotels;
    }
    
    public function getMainImage()
    {
    	    	
    	if(!empty($this->images[0]))
    		$mainImage = $this->images[0];
    	
    	return isset($mainImage) ? $mainImage: null;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Offer
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set isParsed
     *
     * @param boolean $isParsed
     * @return Offer
     */
    public function setIsParsed($isParsed)
    {
        $this->isParsed = $isParsed;

        return $this;
    }

    /**
     * Get isParsed
     *
     * @return boolean 
     */
    public function getIsParsed()
    {
        return $this->isParsed;
    }

    /**
     * Set offerText
     *
     * @param string $offerText
     * @return Offer
     */
    public function setOfferText($offerText)
    {
        $this->offerText = $offerText;

        return $this;
    }

    /**
     * Get offerText
     *
     * @return string 
     */
    public function getOfferText()
    {
        return $this->offerText;
    }
}
