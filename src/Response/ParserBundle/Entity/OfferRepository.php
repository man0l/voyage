<?php
namespace Response\ParserBundle\Entity;

use Doctrine\ORM\EntityRepository;

class OfferRepository extends EntityRepository 
{
	function findAllBusOffers()
	{
		
		$em = $this->getEntityManager();
		$qb = $em->createQueryBuilder("p");
		$qb
			->select("p")
			->from("ResponseParserBundle:Offer", "p")
		    ->where("p.name LIKE :name")
		    ->setParameter("name", "%автобус%")		    
		    ;
		
		$query = $qb->getQuery();
		$result = $query->getResult();
		
		print_r($result);
		return $result;
		
	}
}