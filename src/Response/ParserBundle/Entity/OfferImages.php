<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfferImages
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class OfferImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Offer", inversedBy="images")
     * @var unknown
     */
    private $offer;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255)
     */
    private $src;
    
    /**
     * @ORM\Column(name="isMain", type="integer", nullable=true)
     * @var unknown
     */
    private $isMain;
    
    /**
     * @ORM\Column(name="local_src", type="string", length=255)
     * @var unknown
     */
    private $localSrc;
    
    /**
     * @ORM\Column(name="is_parsed", type="boolean", nullable=true)
     * @var unknown
     */
    private $isParsed;

    
    public function __toString()
    {
    	return $this->localSrc;
    }
 
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set offerId
     *
     * @param integer $offerId
     * @return OfferImages
     */
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;

        return $this;
    }

    /**
     * Get offerId
     *
     * @return integer 
     */
    public function getOfferId()
    {
        return $this->offerId;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return OfferImages
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }
     

    /**
     * Set isMain
     *
     * @param integer $isMain
     * @return OfferImages
     */
    public function setIsMain($isMain)
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * Get isMain
     *
     * @return integer 
     */
    public function getIsMain()
    {
        return $this->isMain;
    }

    /**
     * Set offer
     *
     * @param \Response\ParserBundle\Entity\Offer $offer
     * @return OfferImages
     */
    public function setOffer(\Response\ParserBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Response\ParserBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set localSrc
     *
     * @param string $localSrc
     * @return OfferImages
     */
    public function setLocalSrc($localSrc)
    {
        $this->localSrc = $localSrc;

        return $this;
    }

    /**
     * Get localSrc
     *
     * @return string 
     */
    public function getLocalSrc()
    {
        return $this->localSrc;
    }

    /**
     * Set isParsed
     *
     * @param boolean $isParsed
     * @return OfferImages
     */
    public function setIsParsed($isParsed)
    {
        $this->isParsed = $isParsed;

        return $this;
    }

    /**
     * Get isParsed
     *
     * @return boolean 
     */
    public function getIsParsed()
    {
        return $this->isParsed;
    }
}
