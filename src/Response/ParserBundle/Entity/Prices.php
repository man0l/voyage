<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prices
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Prices
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="hotel_id", type="integer")
     */
    private $hotelId;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_at", type="datetime")
     */
    private $dateAt;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="string", length=255)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="room_type", type="string", length=255)
     */
    private $roomType;

    /**
     * @var string
     *
     * @ORM\Column(name="url_address", type="string", length=255)
     */
    private $link;
    
    /**
     * @ORM\Column(name="price_index", type="integer")
     * @var unknown
     */
    private $priceIndex;
    
    
    /**
     * @ORM\Column(name="room_index", type="integer")
     * @var unknown
     */
    private $roomIndex;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotelId
     *
     * @param integer $hotelId
     * @return Prices
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    /**
     * Get hotelId
     *
     * @return integer 
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Prices
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }
 

    /**
     * Set price
     *
     * @param string $price
     * @return Prices
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set roomType
     *
     * @param string $roomType
     * @return Prices
     */
    public function setRoomType($roomType)
    {
        $this->roomType = $roomType;

        return $this;
    }

    /**
     * Get roomType
     *
     * @return string 
     */
    public function getRoomType()
    {
        return $this->roomType;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Prices
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set date_at
     *
     * @param \DateTime $dateAt
     * @return Prices
     */
    public function setDateAt($dateAt)
    {
        $this->dateAt = $dateAt;

        return $this;
    }

    /**
     * Get date_at
     *
     * @return \DateTime 
     */
    public function getDateAt()
    {
        return $this->dateAt;
    }

    /**
     * Set priceIndex
     *
     * @param integer $priceIndex
     * @return Prices
     */
    public function setPriceIndex($priceIndex)
    {
        $this->priceIndex = $priceIndex;

        return $this;
    }

    /**
     * Get priceIndex
     *
     * @return integer 
     */
    public function getPriceIndex()
    {
        return $this->priceIndex;
    }

    /**
     * Set roomIndex
     *
     * @param integer $roomIndex
     * @return Prices
     */
    public function setRoomIndex($roomIndex)
    {
        $this->roomIndex = $roomIndex;

        return $this;
    }

    /**
     * Get roomIndex
     *
     * @return integer 
     */
    public function getRoomIndex()
    {
        return $this->roomIndex;
    }
    
    public function buildLink()
    {
    	$link = unserialize($this->link);
    	$linkString = "http://pochivki.xn----ctbrbbjzhptl1be6j.com/reservation_poc.php?";
    	//$linkString .=  	implode("&",$link);
    	
    	foreach($link as $key=>$value) {
    		$linkString .= "$key=$value&";
    	}
    	
    	return $linkString;
    	
    }
}
