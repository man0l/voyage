<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hotel
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Hotel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="hotelText", type="text")
     */
    private $hotelText;

    /**
     * @ORM\Column(name="remoteId", type="integer")
     * @var unknown
     */
	private $remoteId;
	
	/**
	 * @ORM\OneToMany(targetEntity="HotelImages", mappedBy="hotel", cascade={"persist", "remove"})
	 * @var unknown
	 */
	private $images;
	
	/**
	 * @ORM\Column(name="price", type="string", length=255)
	 * @var unknown
	 */
	private $price;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Offer", inversedBy="hotels")
	 * 
	 * @var unknown
	 */
	private $offer;
	
	/**
	 * @ORM\Column(name="website", type="string", length=255)
	 * @var unknown
	 */
	private $website;
	
	/**
	* @ORM\Column(name="is_parsed", type="integer", length=255, nullable=true)
	*/
	private $isParsed;
	
	/**
	 * @ORM\Column(name="titles", type="string", length=255, nullable=true)
	 * @var unknown
	 */
	private $titles;

	
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Hotel
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Hotel
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set hotelText
     *
     * @param string $hotelText
     * @return Hotel
     */
    public function setHotelText($hotelText)
    {
        $this->hotelText = $hotelText;

        return $this;
    }

    /**
     * Get hotelText
     *
     * @return string 
     */
    public function getHotelText()
    {
        return $this->hotelText;
    }

    /**
     * Set remoteId
     *
     * @param integer $remoteId
     * @return Hotel
     */
    public function setRemoteId($remoteId)
    {
        $this->remoteId = $remoteId;

        return $this;
    }

    /**
     * Get remoteId
     *
     * @return integer 
     */
    public function getRemoteId()
    {
        return $this->remoteId;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add images
     *
     * @param \Response\ParserBundle\Entity\Hotel $images
     * @return Hotel
     */
    public function addImage(\Response\ParserBundle\Entity\Hotel $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Response\ParserBundle\Entity\Hotel $images
     */
    public function removeImage(\Response\ParserBundle\Entity\Hotel $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Hotel
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set offer
     *
     * @param \Response\ParserBundle\Entity\Offer $offer
     * @return Hotel
     */
    public function setOffer(\Response\ParserBundle\Entity\Offer $offer = null)
    {
        $this->offer = $offer;

        return $this;
    }

    /**
     * Get offer
     *
     * @return \Response\ParserBundle\Entity\Offer 
     */
    public function getOffer()
    {
        return $this->offer;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Hotel
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }
    
   public function getMainImage()
    {
    	     
    	if(!empty($this->images[0]))
    		$mainImage = $this->images[0];
    	 
    	return isset($mainImage) ? $mainImage: null;
    }

    /**
     * Set localSrc
     *
     * @param string $localSrc
     * @return Hotel
     */
    public function setLocalSrc($localSrc)
    {
        $this->localSrc = $localSrc;

        return $this;
    }

    /**
     * Get localSrc
     *
     * @return string 
     */
    public function getLocalSrc()
    {
        return $this->localSrc;
    }

    /**
     * Set isParsed
     *
     * @param integer $isParsed
     * @return Hotel
     */
    public function setIsParsed($isParsed)
    {
        $this->isParsed = $isParsed;

        return $this;
    }

    /**
     * Get isParsed
     *
     * @return integer 
     */
    public function getIsParsed()
    {
        return $this->isParsed;
    }

    /**
     * Set titles
     *
     * @param string $titles
     * @return Hotel
     */
    public function setTitles($titles)
    {
        $this->titles = $titles;

        return $this;
    }

    /**
     * Get titles
     *
     * @return string 
     */
    public function getTitles()
    {
        return unserialize($this->titles);
    }
}
