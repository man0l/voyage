<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HotelImages
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class HotelImages
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Hotel", inversedBy="images", cascade={"persist", "remove"})
     */
    private $hotel;
    
    
    
    

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=255)
     */
    private $src;
    
    /**
     * @ORM\Column(name="local_src", type="string", length=255)
     * @var unknown
     */
    private $localSrc;
    
    /**
     * @ORM\Column(name="is_parsed", type="boolean", nullable=true)
     * @var unknown
     */
    private $isParsed;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hotelId
     *
     * @param integer $hotelId
     * @return HotelImages
     */
    public function setHotelId($hotelId)
    {
        $this->hotelId = $hotelId;

        return $this;
    }

    /**
     * Get hotelId
     *
     * @return integer 
     */
    public function getHotelId()
    {
        return $this->hotelId;
    }

    /**
     * Set src
     *
     * @param string $src
     * @return HotelImages
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string 
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set hotel
     *
     * @param \Response\ParserBundle\Entity\Hotel $hotel
     * @return HotelImages
     */
    public function setHotel(\Response\ParserBundle\Entity\Hotel $hotel = null)
    {
        $this->hotel = $hotel;

        return $this;
    }

    /**
     * Get hotel
     *
     * @return \Response\ParserBundle\Entity\Hotel 
     */
    public function getHotel()
    {
        return $this->hotel;
    }
    
 

    /**
     * Set localSrc
     *
     * @param string $localSrc
     * @return HotelImages
     */
    public function setLocalSrc($localSrc)
    {
        $this->localSrc = $localSrc;

        return $this;
    }

    /**
     * Get localSrc
     *
     * @return string 
     */
    public function getLocalSrc()
    {
        return $this->localSrc;
    }

    /**
     * Set isParsed
     *
     * @param boolean $isParsed
     * @return HotelImages
     */
    public function setIsParsed($isParsed)
    {
        $this->isParsed = $isParsed;

        return $this;
    }

    /**
     * Get isParsed
     *
     * @return boolean 
     */
    public function getIsParsed()
    {
        return $this->isParsed;
    }
}
