<?php

namespace Response\ParserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WebDocument
 *
 * @ORM\Table(name="WebDocument", options={"collate"="cp1251_general_ci", "charset"="cp1251", "engine"="InnoDB"})
 * @ORM\Entity
 */
class WebDocument
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="anchor_text", type="text", nullable=true)
     */
    private $anchorText;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="blob")
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    public function __construct() {
    	
    	$this->createdAt = new \DateTime ;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return WebDocument
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set anchorText
     *
     * @param string $anchorText
     * @return WebDocument
     */
    public function setAnchorText($anchorText)
    {
        $this->anchorText = $anchorText;

        return $this;
    }

    /**
     * Get anchorText
     *
     * @return string 
     */
    public function getAnchorText()
    {
        return $this->anchorText;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return WebDocument
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set body
     *
     * @param string $body
     * @return WebDocument
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return blob 
     */
    public function getBody()
    {
        return $this->body;
        //$content = stream_get_contents($this->body);
        //return $content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return WebDocument
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
