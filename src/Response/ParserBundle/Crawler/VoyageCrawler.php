<?php

namespace Response\ParserBundle\Crawler;
	
use Arachnid\Crawler;
use Response\ParserBundle\Entity\Destination;
use Doctrine\ORM\EntityManager;
use Response\ParserBundle\Entity\Offer;
use Response\ParserBundle\Entity\OfferImages;
use Response\ParserBundle\Entity\Hotel;
use Response\ParserBundle\Entity\HotelImages;
use Doctrine\Common\Collections\ArrayCollection;
use Response\ParserBundle\Entity\Prices;
						 
	class VoyageCrawler extends Crawler
	{
		private $manager;
		private $iterate = 0;
		
		function extractTitleInfo(\Symfony\Component\DomCrawler\Crawler $crawler, $url) 
		{
			echo "parsing ".$url ."\n\n";
			
			if(strpos($url, 'poci') !== false) {
				
				//$this->parseDestination($crawler, $url);
				$this->parseOffers($crawler, $url);
				/* echo "start parsing hotels\n\n";
				$this->parseHotels($crawler, $url);
				echo "end parsing hotels\n\n"; */
				
			} else if(strpos($url, 'hotel') !== false) 
			{
				
				//$this->parseHotelsDetails($crawler, $url);
				//$this->parsePrices($crawler, $url);
			} 
			
			if(($this->iterate % 20) == 0)
			{
				//$this->manager->flush();
				//$this->manager->clear();
			}
			
			$this->iterate++;
		}
		
		function parsePrices($crawler, $url)
		{
			
			$tablePrices = array();
			$links = array();
			
			$crawler->filter(".tablcont")->each(function($node, $i) use(&$tablePrices) {
				 
			 		/* if($i % 2)
			 			$i--; */
				 
					if($subNode = $node->filter(".tabltitle td[style^=color]"))
					 $subNode->each(function($node, $j) use(&$tablePrices, $i) {
						// cycle but once!
					 	$tablePrices[$i]['roomType'] = $node->text();
					});

					if($subNode = $node->filter("tr td[bgcolor='dddddd']"))
					$subNode->each(function($subNode, $j) use(&$tablePrices, $i) {
						$tablePrices[$i]['dates'][] = trim($subNode->text(), chr(0xC2).chr(0xA0)."'");
						
					});
					
					//array_pop($tablePrices[$i]['dates']);
					
					if($subNode = $node->filter("tr.tabltitle td[style^=width]"))
					$subNode->each(function($subNode, $j) use(&$tablePrices, &$i) {
						$tablePrices[$i - 1]['titles'][] = $subNode->text();
							
					});		
					
					//array_shift($tablePrices[$i]['titles']);
					if($subNode = $node->filter("tr td a.price2"))
					$subNode->each(function($subNode, $j) use(&$tablePrices, &$i) {
						$tablePrices[$i - 1]['links'][] = array(
								'href'	=> $subNode->attr('href'),
								'price'	=> $subNode->text()
								
						); 
							
					});
				 
								
			});
			
			 
			$roomIndex = 0;
			foreach($tablePrices as $prices)
			{
				//$price = new Prices;
				//$roomType = array_shift($prices['titles']);
				$roomType = $prices['roomType'];
				$iterator = sizeof($prices['dates']);
				$titlesSize = sizeof($prices['titles']);
				 
				
				for($i = 0; $i < $iterator; $i++)
				{
					$links[$i] = array_slice($prices['links'], $i * $titlesSize, $titlesSize);
					 
				}
				
				
				for($i = 0; $i < sizeof($links); $i++)
				{
					

					for($j = 0; $j < sizeof($links[$i]); $j++) {
					
						$links[$i][$j]['href'] = str_replace("'", "", $links[$i][$j]['href']);
						$links[$i][$j]['href'] = str_replace("http://pochivki.xn----ctbrbbjzhptl1be6j.com/reservation_poc.php?", "", $links[$i][$j]['href']);
							
						//$prices['dates'][$i] = mysql_real_escape_string(str_replace("'","",$prices['dates'][$i]));
						//$links[$i][$j]['price'] = mysql_real_escape_string($links[$i][$j]['price']);
						 
						//$prices['titles'][$j] = mysql_real_escape_string($prices['titles'][$j]);
						
						// &nbsp; is \xa0"						 
						$prices['dates'][$i] = trim($prices['dates'][$i], chr(0xC2).chr(0xA0));
						list($d, $m, $y) = explode(".", $prices['dates'][$i]);
						
						$dateAt = sprintf("%s-%s-%s", $y, $m, $d);
					
						//$roomType = mysql_real_escape_string($roomType);					
						$roomType = trim($roomType);						 
						preg_match("~(\w\s?)*~i", $roomType, $match);
					 	$roomType = $match[0];						
					 		
					 	parse_str($links[$i][$j]['href'], $params);
					 	
					 	// replace the text
					 	$params['ntab'] = $prices['titles'][$j] . " ".$roomType;
					 	$params['ncena'] = $links[$i][$j]['price'];
					 	
					 	//$params = mysql_real_escape_string(serialize($params)); 	
					 	$params = addslashes(serialize($params));
						 
					   $sql = sprintf("INSERT INTO  Prices (
							`id` ,
							`hotel_id` ,
							`title` ,
							`date_at` ,
							`price` ,
							`room_type` ,
							`url_address`,
					   		`price_index`,
					   		`room_index`
							)
							VALUES (
							NULL ,  '%s', 
						 			'%s',
						 			'%s 00:00:00',  
						 			'%s',  
						 			'%s',
						 			'%s',
					   				'%d',
					   				'%d'
							)",
					   		$this->hotelId,
							$prices['titles'][$j],
							$dateAt,
							$links[$i][$j]['price'],
							$roomType,
							$params,
					   		$i,
					   		$roomIndex
							);
						 
						
						$stmt = $this->dbConnection->query($sql);
						 
					}
					
				}
				
				$roomIndex++;
				 
			}
		    
		}
		
		function parseHotelsDetails($crawler, $url)
		{
			
			// location
			
			
			$node = $crawler->filterXPath('//*[@id="printReady"]/text()')->each(function($node, $i) use (&$paragraphs) {
				if(strlen(trim($node->text())) > 10)
					$paragraphs[] = $node->text();
				
			});
			
			
			
			array_shift($paragraphs);
			$outputHTML = "";
			$titles = array(
				'������������',
				'� ������',
				'�� ����������� �� ������',
				'�������'					
			);
			
			$i = 0;
			foreach($paragraphs as $p) {
			
				$outputHTML .= $p."\n\n"; 

				$i++;
			}			
			 
			 
				$hotelName = $crawler->filter('.hotel');
				// get offer
				if(preg_match("/p=(\d+)/", $url, $matchOffer)) {
				 
					$remoteId = $matchOffer[1];
					
					 
					//$offerRep = $this->manager->getRepository('ResponseParserBundle:Offer');
					
					//$offer = $offerRep->findBy(array('remote_id' => $remoteId));
					
					$stmt = $this->dbConnection->query("SELECT id FROM Offer WHERE remote_id = '$remoteId'");					
					 
					$offerDb = $stmt->fetchAll();
					
					if($offerDb){
						
						$offerId = $offerDb[0]['id'];
						
						$hotelImages = array();
	
						$img = $crawler->filter("#printReady > table:nth-child(8) > tbody > tr > td > center > img");
						 
						//$hotelImages[] = $mainImage;
						$img = $crawler->filter(".border");
						if($img && $img->first())
							$hotelImages[] = $img->first()->attr('src');
						 
						$crawler->filter(".cropme img")->each(function($node, $i) use (&$hotelImages) {
							$hotelImages[] = $node->attr("src");
						});
						
						if(preg_match("/h=(\d+)/", $url, $match)) {
						
								/* $hotelRep = $this->manager->getRepository('ResponseParserBundle:Hotel');
								$hotelEntity = $hotelRep->findOneBy(array('remoteId' => $match[1], 'offer_id' => $offerId)); */
								
								$sql = "SELECT id FROM Hotel WHERE remoteId = '{$match[1]}' AND offer_id = '$offerId'";
								$stmt = $this->dbConnection->query($sql);
								
								$hotelDb = $stmt->fetchAll();
								
								
								//$hotelEntity = $hotelRep->findOneBy(array('remoteId' => $match[1], 'offer' => $offer));
								 
								if(isset($hotelDb)) {
									
									$hotelId = $hotelDb[0]['id'];
									/* $hotelEntity->setHotelText(implode("\n", $paragraphs));
						
									$this->manager->persist($hotelEntity);
									$this->manager->flush(); */
									
									$hotelText = $outputHTML;
									//$hotelText = mysql_real_escape_string($hotelText);
									$website = $crawler->filter("#printReady > center > a")->text();
									
									$sql = "UPDATE Hotel SET hotelText = '$hotelText', url='$url', website='$website' WHERE id = '$hotelId' AND offer_id = '{$offerId}'";
									$stmt = $this->dbConnection->query($sql);
									 
									
								} else 
								{
									// save the hotel
									
									$title = $crawler->filter("#printReady > center > font")->text();
									$website = $crawler->filter("#printReady > center > a")->text();
									
									//$title = mysql_real_escape_string($title);
									//$website =  mysql_real_escape_string($website);								 
									//$url = mysql_real_escape_string($url);
									$hotelText = $outputHTML;
									//$hotelText = mysql_real_escape_string($hotelText);
									
									$sql = "INSERT INTO Hotel SET
										name      = '{$title}',
										remoteId = '{$remoteId}',
									    website  = '$website',
										url       = '$url',
										offer_id  = '{$offerId}',
										hotelText = '$hotelText'
									";					
									
									
									$exec = $this->dbConnection->query($sql);
									
								 
									
									$lastInsertId = $this->dbConnection->lastInsertId();
								}
								
								if($hotelImages) {
									
									$hotelId = isset($lastInsertId) ? $lastInsertId : $hotelId;
									
									$sql = "DELETE FROM HotelImages WHERE hotel_id = '$hotelId'";
									$this->dbConnection->query($sql);
									 
									foreach($hotelImages as $image)
									{
										/* $hotelImage = new HotelImages();
										 $hotelImage->setSrc($image);
										$hotelImage->setHotel($hotel);
									
										$this->manager->persist($hotelImage);
										$this->manager->flush(); */
										
										
										$sql = "INSERT INTO HotelImages SET
										src = '{$image}',
										hotel_id = '$hotelId'
										";
										$this->dbConnection->query($sql);
									}
								}
						
							}
						 
					}
					
					$this->hotelId = isset($lastInsertId) ? $lastInsertId : $hotelId;
				}
		}
		
		function parseHotels($crawler, $url)
		{
			$hotelsTableQuery = '//*[@id="printReady"]/table[3]/tbody/tr/td[1]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr';
			$hotelsTable = $crawler->filterXPath($hotelsTableQuery);

			$hotelImages = array();
			$hotelLinks = array();
			$remoteIds	 = array();
			$hotelTitles = array();
			$prices = array();
			
			$crawler->filter(".cropme3 a")->each(function($node, $i) use (&$hotelLinks, &$hotelImages, &$remoteIds) {
				$imageNode = $node->filter("img");
			
				$hotelImages[$i] = $imageNode->attr('src');
				$hotelLinks[$i]  = $node->attr('href');
				$remoteIds[$i]   = $hotelLinks[$i];
				if(preg_match("/h=(\d+)/", $hotelLinks[$i], $match))
					$remoteIds[$i] = $match[1];
				
				$attr = $node->attr('onclick');
				$imageRegEx = "/file=(((?!^jpg).)*\.jpg)/i";
				
				if(preg_match($imageRegEx, $attr, $match))
				{
					$hotelImages[$i] = $match[1];
				}
			});
			
				$crawler->filter("font.oferta")->each(function($node, $i) use (&$prices) {
					$prices[$i] = $node->text();
				});
			
					$crawler->filter("a.oferta")->each(function($node, $i) use (&$hotelTitles) {
						$hotelTitles[$i] = $node->text();
					});
			
			
			
						// remove the first element with font.oferta class
						array_shift($prices);
						// save the hotel in the DB
						$hotelCount = sizeof($hotelLinks);
						
						$hotelRep = $this->manager->getRepository('ResponseParserBundle:Hotel');

						$offerRep = $this->manager->getRepository('ResponseParserBundle:Offer');
						$offer = $offerRep->findOneById($this->offerId);

						
						 
						for($j = 0; $j < $hotelCount; $j++)
						{
							/* $hotel = new Hotel();
							$hotel->setUrl($url);
							$hotel->setName($hotelTitles[$i]);
							$hotel->setRemoteId($remoteIds[$i]);
							$hotel->setPrice($prices[$i]);
								
							$this->manager->persist($hotel);
							$this->manager->flush(); */
							
							
							
							$check = $hotelRep->findOneBy(array('remoteId' => $remoteIds[$j], 'offer' => $offer));
							 
							
							if(!$check) {
								
								//$hotelTitles[$j] = mysql_real_escape_string($hotelTitles[$j]);
								//$remoteIds[$j] = mysql_real_escape_string($remoteIds[$j]);
								//$prices[$j] = mysql_real_escape_string($prices[$j]);
								//$url = mysql_real_escape_string($url);
								
								$sql = "INSERT INTO Hotel SET
											name      = '{$hotelTitles[$j]}',
											remoteId = '{$remoteIds[$j]}',
											price     = '{$prices[$j]}',
											url       = '{$hotelLinks[$j]}',
											offer_id  = '{$this->offerId}'
										";
								
								$exec = $this->dbConnection->query($sql);
								
								echo "insert hotel";
								 
								 
								$lastInsertId = $this->dbConnection->lastInsertId();
								$sql = "DELETE FROM HotelImages WHERE hotel_id = '$lastInsertId'";
								$stmt = $this->dbConnection->query($sql);
								
								echo "delete hotelimages\n\n";
								 
								 
								
							} else 
							{
								// update price!
								
								$sql = "UPDATE Hotel SET price = '$prices[$j]', name='{$hotelTitles[$j]}' WHERE remoteId = '{$remoteIds[$j]}' AND offer_id = '{$this->offerId}'";
								$stmt = $this->dbConnection->query($sql);
								echo "update hotel\n";
								 
							}
						}
		}

		function parseOffers($crawler, $url) 
		{
			preg_match("/p=(\d+)/", $url, $match);
			
			$remoteId = $match[1];
			
			$days = $crawler->filter("font.textver11 font");
			if($days && $days->getNode(0))
			{
				$daysText = $days->getNode(0)->textContent;
			}
			
			
			$imageSource = "";
			$crawler->filter("img.border")->each(function($node, $i) use (&$imageSource) {
				$imageSource = $node->attr('src');
			
			});
					
				$images = array();
			
				$crawler->filter(".textver11 a")->each(function($node, $i) use (&$images) {
					$attr = $node->attr('onclick');
					$imageRegEx = "/file=(((?!^jpg).)*\.jpg)/i";
						
					if(preg_match($imageRegEx, $attr, $match))
					{
						$images[] = $match[1];
					}
				} );
				$onclick = $crawler->filterXPath("a[contains(@onclick, 'Nwin')]");
			
			
				$rep = $this->manager->getRepository("ResponseParserBundle:Offer");
				$offer = $rep->findOneBy(array('remoteId' => $remoteId));
				
				$offerName =  $crawler->filter("#printReady > p > b")->text();
				$textOffer =  $crawler->filterXPath('//*[@id="printReady"]/font[2]/b')->text();
				$price     =  $crawler->filter("#printReady > font.oferta")->text();
					
				if(!$offer) {
					// create new offer
					
						
				/* 	$offerName = iconv($encoding, "UTF-8", $offerName);
					$textOffer = iconv($encoding, "UTF-8", $textOffer);
					$price = iconv($encoding, "UTF-8", $price); */
			
					$offer = new Offer();
						
					$offer->setDestination($this->destination);
					$offer->setDays($daysText);
					$offer->setRemoteId($remoteId);
					$offer->setName($offerName);
					$offer->setTextOffer($textOffer);
					$offer->setPrice($price);
						
					//$daysText = mysql_real_escape_string($daysText);
					$remoteId = intval($remoteId);
					//$offerName = mysql_real_escape_string($offerName);
					//$textOffer = mysql_real_escape_string($textOffer);
					//$price = mysql_real_escape_string($price);
						
					// insert here
					$sql = "INSERT INTO Offer SET 
								destination_id = '{$this->destId}',
								days = '$daysText',
								remote_id = '$remoteId',
								name =	'$offerName',
								textOffer ='$textOffer',
								price = '$price',
								created_at = NOW()
						";
					
					$sql = "INSERT INTO Offer SET
					destination_id = ?,
					days = ?,
					remote_id = ?,
					name =	?,
					textOffer = ?,
					price = ?,
					url = ?
					created_at = NOW()
					";
					
					$stmt = $this->dbConnection->prepare($sql);
					$stmt->bindValue(1, $this->destId);
					$stmt->bindValue(2, $daysText);
					$stmt->bindValue(3, $remoteId);
					$stmt->bindValue(4, $offerName);
					$stmt->bindValue(5, $textOffer);
					$stmt->bindValue(6, $price);
					$stmt->bindValue(7, $url);
					
					$stmt->execute();
					
					//$exec = $this->dbConnection->query($sql);	
					
					$lastInsertId = $this->dbConnection->lastInsertId();
			
				} 
				 else 
				{
					$sql = "UPDATE Offer SET days = ?, name = ?, textOffer = ?, price = ? WHERE offer_id = ?";
						
					/* $stmt = $this->dbConnection->prepare($sql);
 					$stmt->bindValue(1, $daysText);					
					$stmt->bindValue(2, $offerName);
					$stmt->bindValue(3, $textOffer);
					$stmt->bindValue(4, $price);
					$stmt->bindValue(5, $offer->getId());
					
					$stmt->executeUpdate(); */
					
					$sql = "UPDATE Offer SET 
									days = '$daysText', 
									name = '$offerName', 
									textOffer = '$textOffer',
									price = '$price',
									url	  = '$url' 
							WHERE id = '{$offer->getId()}'";
					
					$this->dbConnection->query($sql);
					
				}
			
				$this->offerId = $offerId = isset($lastInsertId) ? $lastInsertId : $offer->getId();	
					
				/* if(!isset($lastInsertId)) 
				{
					$sql = "UPDATE Offer SET days = '$daysText' WHERE id = '$offerId'";
					$this->dbConnection->query($sql);
				} */
				
				/* $offer->setDays($daysText);
				$offer->addImage($mainImage);
 				*/					
				//$imgCollection = new ArrayCollection();

				$imagesRep = $this->manager->getRepository('ResponseParserBundle:OfferImages');
				
				$sql = "DELETE FROM OfferImages WHERE offer_id ='$offerId'";
				$exec = $this->dbConnection->query($sql);
				
				foreach($images as $image) {
						
					/* $img = new OfferImages();
					$img->setSrc($image);
					 
					$offer->addImage($img); */
					 
					
					//$exec->execute();
					
					//$image = mysql_real_escape_string($image);
					$offerId = intval($offerId);
					
					$sql = "INSERT INTO OfferImages SET src = '{$image}', offer_id = '$offerId'";
					$exec = $this->dbConnection->query($sql);
					//echo 'image saved.'.print_r($exec,1);					 
					 
				}
				
				if($imageSource) { 
					// save main image
					$sql = "INSERT INTO OfferImages SET src = ?, offer_id = ?, isMain = 1";
					$stmt = $this->dbConnection->prepare($sql);

					$stmt->bindValue(1, $imageSource);
					$stmt->bindValue(2, $offerId);
					$stmt->execute();					 
				}
					
				//$offer->setImages($imgCollection)
					
				//$this->manager->persist($offer);
				
				
				 
			
		}
		
		function parseDestination($crawler, $url)
		{
			$crawler->filter("h1.zaglavie")->each(function($node, $i) use (&$destinationTitle) {
					
				//$heading = iconv($encoding, "UTF-8", $node->text());
					
				$match = preg_split("/-/", $node->text());
				
				 
				
				if(isset($match[2])) {
					$dest = explode(",", $match[2]);
					$fullDest = $match[2];
				} elseif(isset($match[1]))
				{
					$dest = explode(",", $match[1]);
					$fullDest = $match[1];
				}
				
				//print_r($dest);
				
				//array_pop($dest);					
				if(isset($dest[0])) 
				$destinationTitle = explode(" ", trim($dest[1]));
				else $destinationTitle[0] =  $dest[1];
				
				//$destinationTitle = $destinationTitle[0];
				$destinationTitle = $fullDest;
				
				//$destinationTitle = $node->text();
			
			});
				$destRep = $this->manager->getRepository("ResponseParserBundle:Destination");
				// check for destination
				$destination = $destRep->findOneBy(array('name' => $destinationTitle));
				if(!$destination)
				{
					$destination = new Destination();
					$destination->setName($destinationTitle);
					$destination->setDescription($destinationTitle);
						
					$this->manager->persist($destination);
					
					// batch insert for 20 objects
					/* if(($this->iterate % 20) == 0) 
					{
						$this->manager->flush();
						$this->manager->clear();
					} */
					
					$sql = "INSERT INTO Destination SET 
								name = '$destinationTitle',
								description = '$destinationTitle'";
					$this->dbConnection->query($sql);
					$this->destId = $this->dbConnection->lastInsertId();
				} else $this->destId = $destination->getId();
				
				$this->destination = $destination;
		}
		
		function extractTitleInfo1(\Symfony\Component\DomCrawler\Crawler $crawler, $url)
		{
			parent::extractTitleInfo($crawler, $url);
			
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
				$encoding = "windows-1251";
			} else $encoding = "cp1251";
			
			echo "parsing $url\n\r";
			// scrape the content 
			if(strpos($url, 'pochivka-v-kurort') !== false) {
				// get all html content for the offers within this place
				// get destination
				$destinationDescription = $crawler->filter("h1.zaglavie")->text();
				list(,$destinationTitle) = explode(" - ", $destinationDescription);
				$destinationTitle = $destinationTitle;
				
				$rep = $this->manager->getRepository("ResponseParserBundle:Destination");
				$destination = $rep->findOneBy(array('name' => $destinationTitle));
				
				if(!$destination)
				{
					$destination = new Destination();
					$destination->setDescription($destinationDescription);
					$destination->setName($destinationTitle);
					
					$this->manager->persist($destination);
					$this->manager->flush();
				} else 
				{
					// update the description!
					$destination->setDescription($destinationDescription);
					$this->manager->persist($destination);
					$this->manager->flush();
				}
				
				
				$anchorLinks = $crawler->filter("div[id='right_column'] a");
				$offerTexts =  $crawler->filter(".texttah11 .textar11 b");				 
				$offerPrice =  $crawler->filter(".oferta_price");				
				$fetchDate =  $crawler->filter(".texttah11");
				
				
				foreach($fetchDate as $date) {
					
					preg_match("/([0-9]{2}\.?){2}\.[0-9]{4}/", $date->textContent, $match);
					$dates[] = $match[0];
				}
				
				
				$anchorLinks->each(function(\Symfony\Component\DomCrawler\Crawler $node, $i)  use (&$attributes, &$linkTexts){
					 $attributes[] = $node->attr('href');
					 $linkTexts[] =  $node->text();
				});
				
				
			
				
				foreach($offerTexts as $otext) {
					$offerTxt[] = $otext->textContent;
				}
				
				foreach($offerPrice as $price) {
					$oPrice[] = $price->textContent;
				}

				// get the remote id
				
				
				$sizeLinks = sizeof($linkTexts);
				
				for($i = 0; $i < $sizeLinks; $i++)
				{
					if(isset($dates[$i]))
						list($d, $m, $y) = explode(".", $dates[$i]);
					preg_match("/p=(\d+)/", $attributes[$i], $match);
					 
					$date = sprintf("%s-%s-%s", $y, $m, $d);
					$date = new \DateTime($date);
					
					$offer = $rep->findOneBy(array('remoteId' => $match[1]));
					
					if(!$offer) {
					$offer = new Offer(); 
					$offer->setDestination($destination);
					$offer->setName($linkTexts[$i]);
					$offer->setTextOffer($offerTxt[$i]);
					$offer->setDateAt($date);
					$offer->setPrice($oPrice[$i]);
					$offer->setRemoteId($match[1]);
					
					} else {
						// update the price!
						$offer->setPrice($oPrice[$i]);
					}
					 
					$this->manager->persist($offer);
					$this->manager->flush();
					
				}
				
				
				
				
				
			} elseif (strpos($url, 'poci') !== false) {
				
				// force sleep!
				//sleep(2);
				preg_match("/p=(\d+)/", $url, $match);
				
				$remoteId = $match[1];	
				
				$rep = $this->manager->getRepository("ResponseParserBundle:Offer");
				$destRep = $this->manager->getRepository("ResponseParserBundle:Destination");
				
				 
				
				$crawler->filter("h1.zaglavie")->each(function($node, $i) use (&$destinationTitle, $encoding) {
					//preg_match("/(\w+,?)\s*-\s(\w+,?)*-\s(\w+,?)*/", $node->text(), $match);
					//preg_match("/(\w+)\s-(\w+)\s/i", $node->text(), $match);
					
					$heading = iconv($encoding, "UTF-8", $node->text());
				 
					$match = preg_split("/-/", $heading);
					
					if(isset($match[2])) {
					$dest = explode(",", $match[2]);
					} elseif(isset($match[1]))
					{
						$dest = explode(",", $match[1]);
					}  
					array_pop($dest);
					
					//$arr = explode("�������", $dest[0]);
					$destinationTitle = explode(" ", trim($dest[0]));
					 
					   
				});
				
				// check for destination
				$destination = $destRep->findOneBy(array('name' => $destinationTitle[0]));
				if(!$destination) 
				{
					$destination = new Destination();
					$destination->setName($destinationTitle[0]);
					$destination->setDescription($destinationTitle[0]);
					
					$this->manager->persist($destination);
					$this->manager->flush();
				}
				
				
			 
				 
								
				$days = $crawler->filter("font.textver11 font");
				if($days && $days->getNode(0))
				{
					$daysText = $days->getNode(0)->textContent;
				}
				
				
				$imageSource = "";
				$crawler->filter("img.border")->each(function($node, $i) use (&$imageSource) {
					$imageSource = $node->attr('src');	
								
				});
					
				$images = array();
				
				$crawler->filter(".textver11 a")->each(function($node, $i) use (&$images) {
					$attr = $node->attr('onclick');
					$imageRegEx = "/file=(((?!^jpg).)*\.jpg)/i"; 
					
					if(preg_match($imageRegEx, $attr, $match))
					{
						$images[] = $match[1];
					}
				} );
				$onclick = $crawler->filterXPath("a[contains(@onclick, 'Nwin')]");
				
				$mainImage = new OfferImages();
				$mainImage->setSrc($imageSource);
				$mainImage->setIsMain(1);
				
				$offer = $rep->findOneBy(array('remoteId' => $remoteId));
			
				if(!$offer) {
					// create new offer
					$offerName =  $crawler->filter("#printReady > p > b")->text();
					$textOffer =  $crawler->filterXPath('//*[@id="printReady"]/font[2]/b')->text();
					$price     =  $crawler->filter("#printReady > font.oferta")->text();
					
					$offerName = iconv($encoding, "UTF-8", $offerName);
					$textOffer = iconv($encoding, "UTF-8", $textOffer);
					$price = iconv($encoding, "UTF-8", $price);
					 
					$offer = new Offer();
					
					$offer->setDestination($destination);
					$offer->setDays($daysText);
					$offer->setRemoteId($remoteId);
					$offer->setName($offerName);
					$offer->setTextOffer($textOffer);
					$offer->setPrice($price);
					
				  
				}
				
				 
					
					$offer->setDays($daysText);
					$offer->addImage($mainImage);
					
					//$imgCollection = new ArrayCollection();
					
					foreach($images as $image) {
					 	
						$img = new OfferImages();
						$img->setSrc($image);
						//$img->setOffer($offer);
						$offer->addImage($img);
						/* $this->manager->persist($img);
						$this->manager->flush($img);	 */
					}
					
					//$offer->setImages($imgCollection)
					
					$this->manager->persist($offer);
					$this->manager->flush();
					 
					 // save hotels
					 $hotelsTableQuery = '//*[@id="printReady"]/table[3]/tbody/tr/td[1]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr';
					 $hotelsTable = $crawler->filterXPath($hotelsTableQuery);
					 
					 
				 	 
					 $crawler->filter(".cropme3 a")->each(function($node, $i) use (&$hotelLinks, &$hotelImages, &$remoteIds) {
					 	 $imageNode = $node->filter("img");
					 	 
					 	 $hotelImages[$i] = $imageNode->attr('src');
					 	 $hotelLinks[$i]  = $node->attr('href');				 	
					 	 $remoteIds[$i]   = $hotelLinks[$i];
					 	 if(preg_match("/h=(\d+)/", $hotelLinks[$i], $match))
					 	 	$remoteIds[$i] = $match[1];
					 });
					 
					 $crawler->filter("font.oferta")->each(function($node, $i) use (&$prices) {					 	
					 	$prices[$i] = $node->text();					 	
					 });
					 
				 	$crawler->filter("a.oferta")->each(function($node, $i) use (&$hotelTitles) {
				 		$hotelTitles[$i] = $node->text();
				 	});
					 

					 
					 // remove the first element with font.oferta class
					 array_shift($prices);
					 // save the hotel in the DB
					 $hotelCount = sizeof($hotelLinks);
					 
					 for($i = 0; $i< $hotelCount; $i++) 
					 {
					 	$hotel = new Hotel();
					 	$hotel->setUrl($url);
					 	$hotel->setName($hotelTitles[$i]);
					 	$hotel->setRemoteId($remoteIds[$i]);
					 	$hotel->setPrice($prices[$i]);
					 	
					 	$this->manager->persist($hotel);
					 	$this->manager->flush();
					 	
					 	foreach($hotelImages as $image) 
					 	{
					 		$hotelImage = new HotelImages();
					 		$hotelImage->setSrc($image);
					 		$hotelImage->setHotel($hotel);
					 		
					 		$this->manager->persist($hotelImage);
					 		$this->manager->flush();
					 	}
					 }
				 		
				 
				 
				
			} else if(strpos($url, 'hotel') !== false) {
				
				//$hotel = $crawler->filter("#printReady")->textContent;
				$text = $crawler->filterXPath('//*[@id="printReady"]/text()');
				 
				$crawler->filterXPath('//*[@id="printReady"]/text()')->each(function($node, $i) use (&$paragraphs) {
					$paragraphs[] = $node->text();
				});
				
				$hotelName = $crawler->filter('.hotel');
				
				if(preg_match("/h=(\d+)/", $url, $match)) {
				
					$hotelRep = $this->manager->getRepository('ResponseParserBundle:Hotel');
					$hotelEntity = $hotelRep->findBy(array('remoteId' => $match[1]));
					
					if($hotelEntity) {
					 
						$hotelEntity->setHotelText(implode("\n", $paragraphs));					
						
						$this->manager->persist($hotelEntity);
						$this->manager->flush();
					}
				
				}
				
			} 
				
		}
		
		public function setManager(EntityManager $manager) 
		{
			$this->manager = $manager;
		}
		
		public function setDbConnection($connection)
		{
			$this->dbConnection = $connection;
		} 
	}
	
	
