<?php

namespace Response\ParserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Response\ParserBundle\Entity\WebDocument;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Goutte\Client;

class CrawlHotelsCommand extends ContainerAwareCommand  
{
	function configure()
	{
		$this
		->setName("parser:hotels")
		->setDescription("Parse the site")
		;
	}
		
	function execute(InputInterface $input, OutputInterface $output)
	{
		$this->manager = $this->getContainer()->get('doctrine')->getManager();
		$rep = $this->manager->getRepository('ResponseParserBundle:Offer');
		$this->dbConnection = $conn = $this->getContainer()->get('database_connection');
		$offers = $rep->findBy(array('isParsed' => NULL), array(), 20);
		
 
		if($offers)
		{
			foreach($offers as $offer)
			{
				  
				if($offer->getUrl())
				{
					$url = $offer->getUrl();
					
					
					$url = "http://pochivki.xn----ctbrbbjzhptl1be6j.com".$url;
					 
					$client = new Client();
					$client->setCharset('windows-1251');
						
					$crawler = $client->request('GET', $url);
					$statusCode = $client->getResponse()->getStatus();
					
					if($statusCode == 200)
					{
						$this->offerId = $offer->getId();
						$this->parseHotels($crawler, $url);
					}
					
					$offer->setIsParsed(true);				
					$this->manager->persist($offer);
				}
			}
			
			$this->manager->flush();
		}
	}
	
	function parseHotels($crawler, $url)
	{
		$hotelsTableQuery = '//*[@id="printReady"]/table[3]/tbody/tr/td[1]/table[1]/tbody/tr[2]/td[2]/table/tbody/tr';
		$hotelsTable = $crawler->filterXPath($hotelsTableQuery);
	
		$hotelImages = array();
		$hotelLinks = array();
		$remoteIds	 = array();
		$hotelTitles = array();
		$prices = array();
			
		$crawler->filter(".cropme3 a")->each(function($node, $i) use (&$hotelLinks, &$hotelImages, &$remoteIds) {
			$imageNode = $node->filter("img");
				
			$hotelImages[$i] = $imageNode->attr('src');
			$hotelLinks[$i]  = $node->attr('href');
			$remoteIds[$i]   = $hotelLinks[$i];
			if(preg_match("/h=(\d+)/", $hotelLinks[$i], $match))
				$remoteIds[$i] = $match[1];
	
			$attr = $node->attr('onclick');
			$imageRegEx = "/file=(((?!^jpg).)*\.jpg)/i";
	
			if(preg_match($imageRegEx, $attr, $match))
			{
				$hotelImages[$i] = $match[1];
			}
		});
				
			$crawler->filter("font.oferta")->each(function($node, $i) use (&$prices) {
				$prices[$i] = $node->text();
			});
					
				$crawler->filter("a.oferta")->each(function($node, $i) use (&$hotelTitles) {
					$hotelTitles[$i] = $node->text();
				});					


				 $crawler->filterXPath('//*[@id="printReady"]/table[4]')->each(function ($node, $i) use (&$offerText) {
                                        $offerText = $node->text();
                                });	

				  
				 
  				/* $crawler->filter('.texttah16 font.textver11')->each(function($node, $i) use (&$tableLabels) {
					$tableLabels[] = $node->text();
				});
				*/
	
					// remove the first element with font.oferta class
					array_shift($prices);
					// save the hotel in the DB
					$hotelCount = sizeof($hotelLinks);
	
					$hotelRep = $this->manager->getRepository('ResponseParserBundle:Hotel');
	
					$offerRep = $this->manager->getRepository('ResponseParserBundle:Offer');
					$offer = $offerRep->findOneById($this->offerId);
	
	
						
					for($j = 0; $j < $hotelCount; $j++)
					{
					
						
						$check = $hotelRep->findOneBy(array('remoteId' => $remoteIds[$j], 'offer' => $offer));
		
									
						if(!$check) {
							$hotelTitles[$j] = htmlspecialchars($hotelTitles[$j], ENT_QUOTES);
							$sql = "INSERT INTO Hotel SET
							name      = '{$hotelTitles[$j]}',
							remoteId = '{$remoteIds[$j]}',
							price     = '{$prices[$j]}',
							url       = '{$hotelLinks[$j]}',
							offer_id  = '{$this->offerId}'
							";
			
							$exec = $this->dbConnection->query($sql);
			
							$lastInsertId = $this->dbConnection->lastInsertId();
							$sql = "DELETE FROM HotelImages WHERE hotel_id = '$lastInsertId'";
							$stmt = $this->dbConnection->query($sql);
			
						 
							
							// update offer with offerText
							if(!empty($offerText)) 
							{
							  $sql = "UPDATE Offer SET offer_text = '$offerText' WHERE id = '{$this->offerId}'";
							  $stmt = $this->dbConnection->query($sql);
							}		
						} else
						{
								// update price!
								$hotelTitles[$j] = htmlspecialchars($hotelTitles[$j], ENT_QUOTES);
								$sql = "UPDATE Hotel SET price = '$prices[$j]', name='{$hotelTitles[$j]}' WHERE remoteId = '{$remoteIds[$j]}' AND offer_id = '{$this->offerId}'";
								$stmt = $this->dbConnection->query($sql);
								 
						}
					}
	 }
}
