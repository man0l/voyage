<?php
namespace Response\ParserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Response\ParserBundle\Entity\WebDocument;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Filesystem\Filesystem;


class SaveImagesCommand extends ContainerAwareCommand
{
	/**
	 * 
	 * @var Doctrine\ORM\EntityManager
	 */
	public $em;
	/**
	 * @var Symfony\Component\Filesystem\Filesystem
	 */
	private $filesystem;
	 
	public function configure()
	{
		$this
		->setName("save:images")
		->setDescription("save the images from the url addresses")
		;
	}
	public function run(InputInterface $input, OutputInterface $output) {
		
		
	    $this->em = $this->getContainer()->get('doctrine')->getManager();	    
	    $this->filesystem = $this->getContainer()->get('filesystem');
	    
	    $webRoot = $this->getContainer()->get('kernel')->getRootDir()."/../www";
	    $saveDir = $webRoot."/uploads/saved_from_web/";
	    
	    
		$rep = $this->em->getRepository("ResponseParserBundle:OfferImages");
		
		//$offerImages = $rep->findAll();
		$offerImages = $rep->findBy(array('isParsed' => NULL), array(), 1000);
		
		$i = 0;
		foreach($offerImages as $image)
		{
			
			if(preg_match("/jpeg|jpg|gif|bmp|png/i", $image->getSrc(), $match)) {
				$name = sprintf("%s.%s",md5($image->getSrc()), $match[0]);
				$savePath = $saveDir . $name;
				 
				if(!file_exists($savePath))
				{
						// save the image from the url address
						$url = $image->getSrc();
						$url = str_replace(" ", "%20", $url);
						$contents = file_get_contents($url);
						 
						if($contents) {
						
							$dir = pathinfo($savePath, PATHINFO_DIRNAME);
						
							if (!is_dir($dir)) {
								if (false === $this->filesystem->mkdir($dir)) {
									throw new \RuntimeException(sprintf(
											'Could not create directory %s', $dir
									));
								}
							}
						
							if($fp = fopen($savePath, "w"))
							{
								fwrite($fp, $contents);
								fclose($fp);
							}
						
						}
				 } else {
				 	 
				 }
				 
				 $image->setLocalSrc($name);
				 $image->setIsParsed(true);
				 $this->em->persist($image);
				
				 if(($i % 20) == 0)
				 {
				 	$this->em->flush();
				 }
			}
			
			$i++;
		}
		
		$this->em->flush();
		
		$this->saveHotelImages($saveDir);
		
		
	}
	
	public function saveHotelImages($saveDir)
	{

		$rep = $this->em->getRepository("ResponseParserBundle:HotelImages");
		//$hotelImages =  $rep->findAll();
		$hotelImages = $rep->findBy(array('isParsed' => NULL), array(), 1000);
		
		$i = 0;
		foreach($hotelImages as $image)
		{
			if(preg_match("/jpeg|jpg|gif|bmp|png/i", $image->getSrc(), $match)) {
				
				 
				$name = sprintf("%s.%s",md5($image->getSrc()), $match[0]);
				$savePath = $saveDir . $name;
			
				if(!file_exists($savePath))
				{
					// save the image from the url address
					$url = $image->getSrc();
					$url = str_replace(" ", "%20", $url);
					$contents = file_get_contents($url);
						
				 
					if($contents) {
			
						$dir = pathinfo($savePath, PATHINFO_DIRNAME);
			
						if (!is_dir($dir)) {
							if (false === $this->filesystem->mkdir($dir)) {
								throw new \RuntimeException(sprintf(
										'Could not create directory %s', $dir
								));
							}
						}
			
						if($fp = fopen($savePath, "w"))
						{
							fwrite($fp, $contents);
							fclose($fp);
						}
			
					}
				} else 
				{
					
				}
					
				$image->setLocalSrc($name);
				$image->setIsParsed(true);
				$this->em->persist($image);
				if(($i % 20) == 0)
				{
					$this->em->flush();
				}
			}
			
			$i++;
		}
		
		$this->em->flush();
	}
	
	

		
}