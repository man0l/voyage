<?php
	
	namespace Response\ParserBundle\Command;
	
	use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
	use Symfony\Component\Console\Input\InputInterface;
	use Symfony\Component\Console\Output\OutputInterface;
	use Response\ParserBundle\Entity\WebDocument;
	use Symfony\Component\DependencyInjection\ContainerAwareInterface;
	use Symfony\Component\DependencyInjection\ContainerInterface;
	use Doctrine\DBAL\Types\Type;
	
	include_once __DIR__."/../../../../vendor/cuab/phpcrawl/libs/PHPCrawler.class.php";
	
	class CrawlCommand extends ContainerAwareCommand 
	{
		 function configure() 
		 {
		 	$this
		 		->setName("parser:crawl")
		 		->setDescription("Parse the site")
		 		;
		 	
		 	
		 	
		 	
		 }
		 
		 function execute(InputInterface $input, OutputInterface $output) 
		 {
		 	
			 	$em = $this->getContainer()->get('doctrine.orm.entity_manager');
			 	
			 	if (!Type::hasType('blob'))
			 	{
			 		Type::addType('blob', 'Response\ParserBundle\Doctrine\Type\Blob\Blob');
			 		$em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('blob','blob');
			 	}
		 	
		 		$url = "http://pochivki.xn----ctbrbbjzhptl1be6j.com";
		 		
				$entity = new WebDocument;
				$entityManager = $this->getContainer()->get('doctrine')->getManager();
				
				$crawler = new MyCrawler();
				$crawler->setURL($url);
				$crawler->setEntity($entity);
				$crawler->setEntityManager($entityManager);
				$crawler->setContainer($this->getContainer());
				

				// Only receive content of files with content-type "text/html"
				$crawler->addContentTypeReceiveRule("#text/html#");
				
				// Ignore links to pictures, dont even request pictures
				$crawler->addURLFilterRule("#\.(jpg|jpeg|gif|png)$# i");
				
				// Store and send cookie-data like a browser does
				$crawler->enableCookieHandling(true);
				
				// Set the traffic-limit to 1 MB (in bytes,
				// for testing we dont want to "suck" the whole site)
				$crawler->setTrafficLimit(1000 * 1024);
				
				// Thats enough, now here we go
				$crawler->go();
				
				// At the end, after the process is finished, we print a short
				// report (see method getProcessReport() for more information)
				$report = $crawler->getProcessReport();
				
		 }
		
	}
	
	

	class MyCrawler extends \PHPCrawler implements ContainerAwareInterface {
	
	private $container;
	private $entity;
	private $entityManager;
	
	public function handleDocumentInfo(\PHPCrawlerDocumentInfo $DocInfo){
			
		 
		$lb = "\n";
			// Print the URL and the H	TTP-status-Code
			echo "Page requested: ".$DocInfo->url." (".$DocInfo->http_status_code.")".$lb;
		  
			// Print the refering URL
			echo "Referer-page: ".$DocInfo->referer_url.$lb;
		  
			// Print if the content of the document was be recieved or not
			if ($DocInfo->received == true)
				echo "Content received: ".$DocInfo->bytes_received." bytes".$lb;
			else
				echo "Content not received".$lb;
				
		 	 $em = $this->container->get('doctrine')->getManager();
		     $entity = new WebDocument;
			 
			$entity->setUrl($DocInfo->url);
			$entity->setAnchorText($DocInfo->refering_linktext);
			
			//$txt = htmlspecialchars($DocInfo->content, ENT_QUOTES);
			//$txt = mysql_real_escape_string($DocInfo->content);
				
			
			$entity->setBody($DocInfo->content);
			
			$em->persist($entity);
			$em->flush();
			
			//echo $DocInfo->content, "\n\n";
			echo $entity->getBody();
	
		}
		
	public function setEntity(WebDocument $entity) {
		$this->entity = $entity;
	}
	
	public function setEntityManager($entityManager) {
		$this->entityManager = $entityManager;
	}
	
	public function setContainer(ContainerInterface $container = null)
	{ 
		$this->container = $container;
	}
	
	
	public function getContainer()
	{ 
		return $this->container; 
	}
	
	}