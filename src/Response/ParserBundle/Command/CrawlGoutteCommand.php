<?php

namespace Response\ParserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Response\ParserBundle\Entity\WebDocument;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Response\ParserBundle\Crawler\VoyageCrawler;


class CrawlGoutteCommand extends ContainerAwareCommand  
{
		function configure()
		{
			$this
			->setName("parser:scrape")
			->setDescription("Parse the site")
			;
		}
			
		function execute(InputInterface $input, OutputInterface $output)
		{
			
			$crawler = new VoyageCrawler('http://pochivki.xn----ctbrbbjzhptl1be6j.com', 2); 
			$crawler->setManager($this->getContainer()->get('doctrine')->getManager());
			$crawler->setDbConnection($this->getContainer()->get('database_connection'));
			$crawler->traverse();
			
			// Get link data
			$links = $crawler->getLinks();
			//print_r($links);
		}
}
