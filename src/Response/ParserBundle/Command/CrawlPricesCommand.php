<?php 


namespace Response\ParserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Response\ParserBundle\Entity\WebDocument;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Response\ParserBundle\Crawler\VoyageCrawler;
use Goutte\Client;
use Response\ParserBundle\Entity\Prices;

class CrawlPricesCommand extends ContainerAwareCommand
{
	
	function configure()
	{
		$this
		->setName("parser:prices")
		->setDescription("Parse the site")
		;
	}
	
	function run(InputInterface $input, OutputInterface $output)
	{
		$this->output = $output;
		$em = $this->getContainer()->get('doctrine')->getManager();
		$rep = $em->getRepository('ResponseParserBundle:Hotel');
		
		$hotels = $rep->findBy(array('isParsed' => NULL), array(), 200);
		 
		$this->dbConnection = $conn = $this->getContainer()->get('database_connection');
		
		$i = 0;
		if($hotels) 
		foreach($hotels as $hotel)
		{
			if($hotel->getUrl()) 
			{ 
				if(strpos($hotel->getUrl(), 'hotel.php') !== false) {
					
					//echo "parsing hotel URL ". $hotel->getUrl()."\n\n";
					
					if(strpos($hotel->getUrl(), "http") === false)
						$url = "http://pochivki.xn----ctbrbbjzhptl1be6j.com/".$hotel->getUrl();
					else $url = $hotel->getUrl();
					
					$client = new Client();
					$client->setCharset('windows-1251');
						
					$crawler = $client->request('GET', $url);
					$statusCode = $client->getResponse()->getStatus();
					
					if ($statusCode === 200) {
						
						$this->hotelId = $hotel->getId();
						$sql = "DELETE FROM Prices WHERE hotel_id = '{$this->hotelId}'";
						
						$stmt = $conn->query($sql);
						$this->parsePrices($crawler);						
						
						$this->parseHotelsDetails($crawler, $url);
						
					}
				}
				
			}
			
			$hotel->setIsParsed(1);			
			$em->persist($hotel);
			$em->flush();
			
			
			/* $i++;
			
			if(($i % 5) == 0) 
			{
				$em->flush();
			} */
		}
		
		//$em->flush();
		
	}
	
	function parseHotelsDetails($crawler, $url)
	{
			
		// location
			
			
		$crawler->filterXPath('//*[@id="printReady"]/text()')->each(function($node, $i) use (&$paragraphs) {
			
		 
			 if($node) {
			 
				if(strlen(trim($node->text())) > 10)
					$paragraphs[] = $node->text();
			}
			
		 
	
		});
				
			$outputHTML = "";
			if($paragraphs) {	
				array_shift($paragraphs);
				$outputHTML = "";
			 
					
				$i = 0;
				foreach($paragraphs as $p) {
						
					$outputHTML .= $p."\n\n";
		
					$i++;
				}
			}
	
	
			$hotelName = $crawler->filter('.hotel');
		
			// get offer
			if(preg_match("/p=(\d+)/", $url, $matchOffer)) {
					
				$remoteId = $matchOffer[1];
					
	
				//$offerRep = $this->manager->getRepository('ResponseParserBundle:Offer');
					
				//$offer = $offerRep->findBy(array('remote_id' => $remoteId));
					
				$stmt = $this->dbConnection->query("SELECT id FROM Offer WHERE remote_id = '$remoteId'");
	
				$offerDb = $stmt->fetchAll();
					
				if($offerDb){
	
					$offerId = $offerDb[0]['id'];
	
					$hotelImages = array();	
					
						
					/*$crawler->filter(".cropme img")->each(function($node, $i) use (&$hotelImages) {
						$hotelImages[] = $node->attr("src");
						//print_r($node->attr('src'));
					});
					*/
					
					$crawler->filter(".cropme a")->each(function($node, $i) use (&$hotelImages) {
						 
						$attr = $node->attr('onclick');
						$imageRegEx = "/file=(((?!^jpg).)*\.jpg)/i";
				
						if(preg_match($imageRegEx, $attr, $match))
						{
							$hotelImages[$i] = $match[1];
							 
						}
						
						 
						
					});
						
						$website = "";
						$crawler->filter("#printReady > center > a")->each(function($node, $i) use (&$website) {
							$website = $node->text();
							
						});
						
						$tableLabels = array();
						 $crawler->filter('.texttah16 font.textver11')->each(function($node, $i) use (&$tableLabels) {
                		                        $tableLabels[] = $node->text();
												 
		                                });

						$titles = "";
					 	if(sizeof($tableLabels) > 0)
					 	{
					 		 
					 		$titles = serialize($tableLabels);
					 		 
					 	}
					 
						if(preg_match("/h=(\d+)/", $url, $match)) {
	
							/* $hotelRep = $this->manager->getRepository('ResponseParserBundle:Hotel');
							 $hotelEntity = $hotelRep->findOneBy(array('remoteId' => $match[1], 'offer_id' => $offerId)); */
	
							$sql = "SELECT id FROM Hotel WHERE remoteId = '{$match[1]}' AND offer_id = '$offerId'";
							$stmt = $this->dbConnection->query($sql);
	
							$hotelDb = $stmt->fetchAll();
	
	
							//$hotelEntity = $hotelRep->findOneBy(array('remoteId' => $match[1], 'offer' => $offer));
								
							if(isset($hotelDb)) {
									
								$hotelId = $hotelDb[0]['id'];
								/* $hotelEntity->setHotelText(implode("\n", $paragraphs));
	
								$this->manager->persist($hotelEntity);
								$this->manager->flush(); */
									
								$hotelText = htmlspecialchars($outputHTML, ENT_QUOTES);
							 
								//$hotelText = mysql_real_escape_string($hotelText);
							        	 
									
								$sql = "UPDATE Hotel SET hotelText = '$hotelText', url='$url', website='$website', titles='$titles'  WHERE id = '$hotelId' AND offer_id = '{$offerId}'";
								$stmt = $this->dbConnection->query($sql);
	
									
							} else
							{
								// save the hotel
									
								//$title = $crawler->filter("#printReady > center > font")->text();
								$title = isset($hotelName) ? $hotelName->text() : '';
								
									
								//$title = mysql_real_escape_string($title);
								//$website =  mysql_real_escape_string($website);
								//$url = mysql_real_escape_string($url);
								$hotelText = htmlspecialchars($outputHTML, ENT_QUOTES);
								$title = htmlspecialchars($title, ENT_QUOTES);
								//$hotelText = mysql_real_escape_string($hotelText);
									
								$sql = "INSERT INTO Hotel SET
								name      = '{$title}',
								remoteId = '{$remoteId}',
								website  = '$website',
								url       = '$url',
								offer_id  = '{$offerId}',
								hotelText = '$hotelText',
								titles	  = '$titles'
								";
									
									
								$exec = $this->dbConnection->query($sql);
									
									
									
								$lastInsertId = $this->dbConnection->lastInsertId();
							}
	
							if($hotelImages) {
									
								$hotelId = isset($lastInsertId) ? $lastInsertId : $hotelId;
									
								$sql = "DELETE FROM HotelImages WHERE hotel_id = '$hotelId'";
								$this->dbConnection->query($sql);
	
								foreach($hotelImages as $image)
								{
								/* $hotelImage = new HotelImages();
								$hotelImage->setSrc($image);
									$hotelImage->setHotel($hotel);
									 	
									$this->manager->persist($hotelImage);
									$this->manager->flush(); */
	
	
									$sql = "INSERT INTO HotelImages SET
									src = '{$image}',
									hotel_id = '$hotelId'
								";
								$this->dbConnection->query($sql);
								}
								}
	
								}
									
								}
									
								$this->hotelId = isset($lastInsertId) ? $lastInsertId : $hotelId;
						}
	}
	
	function parsePrices($crawler)
	{
			
		$tablePrices = array();
		$links = array();
		
		$conn = $this->getContainer()->get('database_connection');
		$em = $this->getContainer()->get('doctrine')->getManager(); 
		
		$rootDir = $this->getContainer()->get('kernel')->getRootDir();
		$rootDir .= "/../hotel_prices_sql/";
		$fileName = $rootDir . "prices_import_".uniqid().".sql";
		$fp = fopen($fileName, "w");
		
		if(!$fp)
			throw new Exception("the output SQL file cannot be created");
			
		$crawler->filter(".tablcont")->each(function($node, $i) use(&$tablePrices, $conn, $em, $fp) {
				
			/* if($i % 2)
			 $i--; */
				
			if($subNode = $node->filter(".tabltitle td[style^=color]"))
				$subNode->each(function($node, $j) use(&$tablePrices, $i) {
				// cycle but once!
			$tablePrices[$i]['roomType'] = $node->text();
			});
	
			if($subNode = $node->filter("tr td[bgcolor='dddddd']"))
				$subNode->each(function($subNode, $j) use(&$tablePrices, $i) {
				$tablePrices[$i]['dates'][] = trim($subNode->text(), chr(0xC2).chr(0xA0)."'");
	
			});
				
			//array_pop($tablePrices[$i]['dates']);
				
			if($subNode = $node->filter("tr.tabltitle td[style^=width]"))
				$subNode->each(function($subNode, $j) use(&$tablePrices, &$i) {
				$tablePrices[$i - 1]['titles'][] = $subNode->text();
					
			});
				
			//array_shift($tablePrices[$i]['titles']);
			if($subNode = $node->filter("tr td a.price2"))
				$subNode->each(function($subNode, $j) use(&$tablePrices, &$i) {
				$tablePrices[$i - 1]['links'][] = array(
						'href'	=> $subNode->attr('href'),
						'price'	=> $subNode->text()
	
				);
					
			});
				
	
		});
				
	
			$roomIndex = 0;
			foreach($tablePrices as $prices)
			{
				//$price = new Prices;
				//$roomType = array_shift($prices['titles']);
				$roomType = $prices['roomType'];
				$iterator = sizeof($prices['dates']);
				$titlesSize = sizeof($prices['titles']);
					
	
				for($i = 0; $i < $iterator; $i++)
				{
					$links[$i] = array_slice($prices['links'], $i * $titlesSize, $titlesSize);	
				}
	
	
					for($i = 0; $i < sizeof($links); $i++)
					{			
		
						for($j = 0; $j < sizeof($links[$i]); $j++) {
							
							$links[$i][$j]['href'] = str_replace("'", "", $links[$i][$j]['href']);
							$links[$i][$j]['href'] = str_replace("http://pochivki.xn----ctbrbbjzhptl1be6j.com/reservation_poc.php?", "", $links[$i][$j]['href']);
							
							// &nbsp; is \xa0"
							$prices['dates'][$i] = trim($prices['dates'][$i], chr(0xC2).chr(0xA0));
							list($d, $m, $y) = explode(".", $prices['dates'][$i]);
			
							$dateAt = sprintf("%s-%s-%s", $y, $m, $d);
							
										
							//$roomType = mysql_real_escape_string($roomType);
							$roomType = trim($roomType);
							preg_match("~(\w\s?)*~i", $roomType, $match);
							$roomType = $match[0];
				
							parse_str($links[$i][$j]['href'], $params);
								
							// replace the text
							$params['ntab'] = $prices['titles'][$j] . " ".$roomType;
							$params['ncena'] = $links[$i][$j]['price'];
								
							$params = addslashes(serialize($params));
										
								$sql = sprintf("INSERT INTO  Prices (`id` ,`hotel_id`,`title`,`date_at`,`price`,`room_type`,`url_address`,`price_index`,`room_index`) VALUES (NULL ,  '%s','%s','%s 00:00:00','%s','%s','%s','%d','%d');",
							   					$this->hotelId, $prices['titles'][$j],$dateAt,$links[$i][$j]['price'],$roomType,$params,$i,	$roomIndex);
								fwrite($fp, $sql."\n\r");
								//$stmt = $conn->query($sql);
								
								if($dateAt == '--')
								{
									$dateAt = date("Y-m-d H:i:s");
								}
								 
								$dateAt = new \DateTime($dateAt);
								 
								$price = new Prices;
								$price->setHotelId($this->hotelId);
								$price->setTitle($prices['titles'][$j]);
								$price->setDateAt($dateAt);
								$price->setPrice($links[$i][$j]['price']);
								$price->setRoomType($roomType);
								$price->setLink($params);
								$price->setPriceIndex($i);
								$price->setRoomIndex($roomIndex);
								
								//$em->persist($price);
								//$em->flush();
									
						}
						
					}
	
				$roomIndex++;
					
				}
	
		fclose($fp);
	}
	
} 
